/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 19:07:51 by rauer             #+#    #+#             */
/*   Updated: 2024/03/02 19:09:01 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <pthread.h>

int	die(int ts, t_philo *philo)
{
	if (print_status(ts, philo, STATUS_DEAD))
		return (error(philo->shared, 1));
	if (pthread_mutex_lock(&philo->shared->lock))
		return (error(philo->shared, 2));
	philo->shared->died = 1;
	if (pthread_mutex_unlock(&philo->shared->lock))
		return (error(philo->shared, 3));
	return (-1);
}

int	check(t_philo *philo)
{
	int	ts;
	int	r;

	ts = time_since_start();
	if (ts - philo->last_time_eaten >= philo->settings.ttd)
		return (die(ts, philo));
	if (pthread_mutex_lock(&philo->shared->lock))
		return (error(philo->shared, 4));
	r = philo->shared->died || philo->shared->error
		|| philo->shared->philos_finished == philo->settings.nop;
	if (pthread_mutex_unlock(&philo->shared->lock))
		return (error(philo->shared, 5));
	return (r);
}
