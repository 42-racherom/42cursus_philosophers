/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 06:08:01 by racherom          #+#    #+#             */
/*   Updated: 2024/03/02 09:11:46 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/time.h>
#include <unistd.h>

long	time_since_start(void)
{
	static struct timeval	st;
	struct timeval			ct;
	struct timezone			tz;
	long					d;

	gettimeofday(&ct, &tz);
	if (st.tv_sec == 0)
	{
		st = ct;
		return (0);
	}
	d = (ct.tv_sec - st.tv_sec) * 1000;
	d += (ct.tv_usec - st.tv_usec) / 1000;
	return (d);
}

int	wait_til(long t)
{
	long	w;

	w = t - time_since_start() - 50;
	if (w > 0 && usleep(w * 1000))
		return (-1);
	while (time_since_start() < t)
		;
	return (0);
}
