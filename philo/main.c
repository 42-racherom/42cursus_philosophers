/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/13 05:40:02 by racherom          #+#    #+#             */
/*   Updated: 2024/03/02 20:25:22 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

void	prepare_philos(t_app *app)
{
	int	i;

	i = 0;
	while (i < app->s.nop)
	{
		app->p[i].number = i + 1;
		app->p[i].last_time_eaten = 0;
		app->p[i].left = app->f + i;
		app->p[i].right = app->f + ((i + 1) % app->s.nop);
		app->p[i].times_eaten = 0;
		app->p[i].settings = app->s;
		app->p[i++].shared = &app->shared;
	}
}

int	prepare(t_app *app)
{
	int	i;

	if (pthread_mutex_init(&(app->shared.lock), 0))
		return (-1);
	app->shared.philos_finished = 0;
	app->shared.died = 0;
	app->shared.error = 0;
	i = 0;
	while (i < app->s.nop)
	{
		app->f[i].in_use = 0;
		if (pthread_mutex_init(&app->f[i++].mutex, NULL))
			return (-1);
	}
	prepare_philos(app);
	return (0);
}

int	run(t_app *app)
{
	int			i;
	int			r;
	pthread_t	*t;

	i = 0;
	r = 0;
	while (!r && i < app->s.nop)
	{
		t = &(app->p[i].thread);
		r = pthread_create(t, 0, philo_eat_sleep_think_repeat, app->p + i++);
		usleep(1000);
	}
	while (i-- > 0)
		pthread_join(app->p[i].thread, 0);
	return (r);
}

int	clean(t_app *app)
{
	int	i;

	i = 0;
	while (i < app->s.nop)
		pthread_mutex_destroy(&app->f[i++].mutex);
	pthread_mutex_destroy(&app->shared.lock);
	free(app->f);
	free(app->p);
	return (0);
}

int	main(int argc, char **argv)
{
	t_app	app;

	if (parse(&app.s, argc, argv))
		return (1);
	app.f = malloc(app.s.nop * sizeof(t_fork));
	if (!app.f)
		return (2);
	app.p = malloc(app.s.nop * sizeof(t_philo));
	if (!app.p)
	{
		free(app.f);
		return (3);
	}
	if (prepare(&app))
	{
		free(app.f);
		free(app.p);
		return (4);
	}
	if (run(&app))
		error(&app.shared, 0);
	return (clean(&app));
}
