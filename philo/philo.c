/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/13 07:32:43 by racherom          #+#    #+#             */
/*   Updated: 2024/03/02 20:35:33 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

int	philo_take_right_fork(t_philo *philo)
{
	int	i;
	int	r;

	i = 0;
	r = take_fork(philo->right);
	while (!r && i++ < 500)
	{
		usleep(100);
		r = take_fork(philo->right);
	}
	if (r < 0)
		return (error(philo->shared, r));
	if (!r)
	{
		r = return_fork(philo->left);
		if (r < 0)
			return (error(philo->shared, 9));
		if (print_status(time_since_start(), philo, STATUS_THINKING))
			return (error(philo->shared, 10));
		return (0);
	}
	if (print_status(time_since_start(), philo, STATUS_FORK_TAKEN))
		return (error(philo->shared, 11));
	return (1);
}

int	philo_take_forks(t_philo *philo)
{
	int	r;

	r = take_fork(philo->left);
	if (r < 0)
		return (error(philo->shared, 6));
	if (!r)
		return (0);
	if (print_status(time_since_start(), philo, STATUS_FORK_TAKEN))
		return (error(philo->shared, 7));
	return (philo_take_right_fork(philo));
}

int	philo_eat(t_philo *philo)
{
	int		r;
	long	ts;

	if (check(philo))
		return (-1);
	ts = time_since_start();
	if (print_status(ts, philo, STATUS_EATING))
		return (error(philo->shared, 12));
	philo->last_time_eaten = ts;
	if (wait_til(ts + philo->settings.tte))
		return (error(philo->shared, 13));
	philo->times_eaten++;
	r = 0;
	if (philo->times_eaten == philo->settings.notepme)
	{
		if (pthread_mutex_lock(&philo->shared->lock))
			return (error(philo->shared, 14));
		philo->shared->philos_finished++;
		r = philo->shared->philos_finished == philo->settings.nop;
		if (pthread_mutex_unlock(&philo->shared->lock))
			return (error(philo->shared, 15));
	}
	return (r);
}

int	philo_eat_sleep_think(t_philo *philo)
{
	long	ts;

	if (philo_eat(philo))
		return (-1);
	if (check(philo))
		return (-1);
	if (return_fork(philo->left) + return_fork(philo->right) != 0)
		return (error(philo->shared, 16));
	ts = time_since_start();
	if (print_status(ts, philo, STATUS_SLEEPING))
		return (error(philo->shared, 17));
	if (wait_til(ts + philo->settings.tts))
		return (error(philo->shared, 18));
	if (check(philo))
		return (-1);
	if (print_status(time_since_start(), philo, STATUS_THINKING))
		return (error(philo->shared, 19));
	usleep(100);
	return (0);
}

void	*philo_eat_sleep_think_repeat(void *p)
{
	int		r;
	t_philo	*philo;

	philo = p;
	r = 0;
	while (!r)
	{
		if (check(philo))
			return (0);
		r = philo_take_forks(philo);
		if (r == 1)
			r = philo_eat_sleep_think(philo);
	}
	return (0);
}
