/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/13 05:27:49 by racherom          #+#    #+#             */
/*   Updated: 2024/03/02 19:17:24 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H
# include <pthread.h>

typedef enum e_status
{
	STATUS_THINKING = 0,
	STATUS_FORK_TAKEN = 1,
	STATUS_EATING = 2,
	STATUS_SLEEPING = 3,
	STATUS_DEAD = 4,
	STATUS_ERROR = 5,
	STATUS_FINISH = 6,
	STATUS_TEST = 7
}					t_status;

typedef struct s_fork
{
	int				in_use;
	pthread_mutex_t	mutex;
}					t_fork;

typedef struct s_settings
{
	int				nop;
	int				ttd;
	int				tte;
	int				tts;
	int				notepme;
}					t_settings;

typedef struct s_shared
{
	int				philos_finished;
	int				died;
	int				error;
	pthread_mutex_t	lock;
}					t_shared;

typedef struct s_philo
{
	int				number;
	int				last_time_eaten;
	int				times_eaten;
	t_fork			*left;
	t_fork			*right;
	t_settings		settings;
	t_shared		*shared;
	pthread_t		thread;
}					t_philo;

typedef struct s_app
{
	t_settings		s;
	t_shared		shared;
	t_fork			*f;
	t_philo			*p;
}					t_app;

int					error(t_shared *s, int i);
int					parse(t_settings *s, int argc, char **argv);
void				*philo_eat_sleep_think_repeat(void *p);
long				time_since_start(void);
int					wait_til(long t);
int					take_fork(t_fork *f);
int					return_fork(t_fork *f);
int					print_status(int ts, t_philo *p, t_status s);
int					check(t_philo *philo);
#endif
