/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   status.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/14 02:37:05 by rauer             #+#    #+#             */
/*   Updated: 2024/03/02 19:27:53 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

int	error(t_shared *s, int i)
{
	if (!pthread_mutex_lock(&s->lock))
	{
		s->error = 1;
		pthread_mutex_unlock(&s->lock);
	}
	printf("ERROR %i\n", i);
	return (-1);
}

char	*status_string(t_status s)
{
	static char	*status[7];

	if (!status[0])
	{
		status[0] = "is thinking";
		status[1] = "has taken a fork";
		status[2] = "is eating";
		status[3] = "is sleeping";
		status[4] = "died";
		status[5] = "Error";
		status[6] = "Test";
	}
	if (s < 0 || s > 6)
		s = STATUS_ERROR;
	return (status[s]);
}

int	print_status(int ts, t_philo *philo, t_status status)
{
	if (status < 0 || status > 6)
		return (-1);
	printf("%i %i %s\n", ts, philo->number, status_string(status));
	return (0);
}
