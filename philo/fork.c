/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/13 06:58:35 by racherom          #+#    #+#             */
/*   Updated: 2024/03/02 19:29:28 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <pthread.h>
#include <stdlib.h>
#include <sys/time.h>

int	take_fork(t_fork *f)
{
	int	r;

	if (pthread_mutex_lock(&f->mutex))
		return (-1);
	r = !f->in_use;
	f->in_use = 1;
	if (pthread_mutex_unlock(&f->mutex))
		return (-2);
	return (r);
}

int	return_fork(t_fork *f)
{
	if (pthread_mutex_lock(&f->mutex))
		return (-1);
	if (!f->in_use)
		return (-1);
	f->in_use = 0;
	if (pthread_mutex_unlock(&f->mutex))
		return (-1);
	return (0);
}
