/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/07 23:09:32 by racherom          #+#    #+#             */
/*   Updated: 2024/03/02 19:19:24 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <stdio.h>

int	ft_atoi(const char *str)
{
	int	i;
	int	m;

	if (!str)
		return (0);
	i = 0;
	m = 1;
	if (*str == '+')
		str++;
	else if (*str == '-')
	{
		m = -1;
		str++;
	}
	while (*str >= '0' && *str <= '9')
		i = (i * 10) + (m * (*(str++) - '0'));
	return (i);
}

int	print_usage(char *prog)
{
	printf("usage: %s number_of_philosophers time_to_die time_to_eat ", prog);
	printf("time_to_sleep [number_of_times_each_philosopher_must_eat]\n");
	printf("\tnumber_of_philosophers: The number of philosophers and also ");
	printf("the number of forks.\n");
	printf("\ttime_to_die (in milliseconds): If a philosopher didn’t start ");
	printf("eating time_to_die milliseconds since the beginning of their ");
	printf("last meal or the beginning of the simulation, they die.\n");
	printf("\ttime_to_eat (in milliseconds): The time it takes for a ");
	printf("philosopher to eat. During that time, they will need to hold ");
	printf("two forks.\n");
	printf("\ttime_to_sleep (in milliseconds): The time a philosopher will ");
	printf("spend sleeping.\n");
	printf("\tnumber_of_times_each_philosopher_must_eat (optional argument)");
	printf(": If all philosophers have eaten at least ");
	printf("number_of_times_each_philosopher_must_eat times, the simulation ");
	printf("stops. If not specified, the simulation stops when aphilosopher ");
	printf("dies.\n");
	return (0);
}

int	parse(t_settings *s, int argc, char **argv)
{
	if (argc < 5 || argc > 6)
	{
		print_usage(*argv);
		return (1);
	}
	s->nop = ft_atoi(argv[1]);
	s->ttd = ft_atoi(argv[2]);
	s->tte = ft_atoi(argv[3]);
	s->tts = ft_atoi(argv[4]);
	s->notepme = -1;
	if (argc == 6)
		s->notepme = ft_atoi(argv[5]);
	if (s->nop < 1 || s->tte < 0 || s->tts < 0)
		return (2);
	return (0);
}
